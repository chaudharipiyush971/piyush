from flask import Flask

app = Flask(_name_)

@app.route('/')
def home():
    return 'Hello, World!'
@app.route('/new-feature')
def new_feature():
    return 'This is a new feature!'

if _name_ == '_main_':
    app.run(debug=True)
